CREATE TABLE IF NOT EXISTS logs
(
    id              UUID,
    level           CHARACTER VARYING(10),
    created_at       TIMESTAMP WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    service_name    CHARACTER VARYING(30),
    message text,
    PRIMARY KEY (id)
);