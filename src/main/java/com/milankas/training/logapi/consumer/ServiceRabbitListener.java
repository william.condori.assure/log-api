package com.milankas.training.logapi.consumer;

import com.milankas.training.logapi.domain.dto.LogDTO;
import com.milankas.training.logapi.domain.service.LogService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ServiceRabbitListener {

    @Autowired
    private LogService logService;

    @RabbitListener(queues = "${rabbitmq.queque}")
    public void consumeMessageFromQueue(LogDTO logDTO) {
        logService.create(logDTO);
        System.out.println("Message recieved from queue : " + logDTO);
    }
}
