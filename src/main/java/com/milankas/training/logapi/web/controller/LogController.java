package com.milankas.training.logapi.web.controller;

import com.milankas.training.logapi.domain.dto.LogDTO;
import com.milankas.training.logapi.domain.service.LogService;
import com.milankas.training.logapi.web.controller.exception.ApiRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/logs")
public class LogController {

    @Autowired
    private LogService logService;

    @GetMapping("/")
    public ResponseEntity<List<LogDTO>> getAll(
            @RequestParam(required = false) String serviceName,
            @RequestParam(required = false) List<String> level,
            @RequestParam(required = false) String message,
            @RequestParam(required = false) String fromDateTime,
            @RequestParam(required = false) String toDateTime

    ) {
        if (serviceName != null && !serviceName.trim().equals("")) {
            return ResponseEntity.ok(logService.getFindAllByServiceName(serviceName.toLowerCase()));
        }
        if (level != null && level.size() > 0) {
            return ResponseEntity.ok(logService.findAllByLevelIn(level));
        }
        if (message != null && !message.trim().equals("")) {
            return ResponseEntity.ok(logService.findAllByMessageContains(message));
        }
        if (fromDateTime != null && !fromDateTime.trim().isEmpty()) {
            try {
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
                Date fromDateTimeDate = format.parse(fromDateTime);
                Date toDateTimeDate;

                if (toDateTime != null && !toDateTime.trim().isEmpty()) {
                    toDateTimeDate = format.parse(toDateTime);
                } else {
                    toDateTimeDate = new Date();
                }

                if (!fromDateTimeDate.before(toDateTimeDate)) {
                    throw new ApiRequestException("fromDateTime cannot be greater than toDateTime");
                }

                return ResponseEntity.ok(logService.findAllByCreatedAtBetween(fromDateTimeDate, toDateTimeDate));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return ResponseEntity.ok(logService.getAll());
    }
}
