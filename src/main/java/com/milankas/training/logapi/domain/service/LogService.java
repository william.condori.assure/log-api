package com.milankas.training.logapi.domain.service;

import com.milankas.training.logapi.domain.dto.LogDTO;
import com.milankas.training.logapi.persistence.crud.LogCrudRepository;
import com.milankas.training.logapi.persistence.entity.Log;
import com.milankas.training.logapi.persistence.mapper.LogMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class LogService {

    @Autowired
    private LogCrudRepository logRepository;

    @Autowired
    private LogMapper logMapper;

    public List<LogDTO> getAll() {
        return logMapper.toLogDTOList(logRepository.findAll());
    }

    public List<LogDTO> getFindAllByServiceName(String serviceName) {
        return logMapper.toLogDTOList(logRepository.findAllByServiceName(serviceName));
    }

    public List<LogDTO> findAllByLevelIn(List<String> levelList) {
        return logMapper.toLogDTOList(logRepository.findAllByLevelIn(levelList));
    }

    public List<LogDTO> findAllByMessageContains(String message) {
        return logMapper.toLogDTOList(logRepository.findAllByMessageContains(message));
    }

    public List<LogDTO> findAllByCreatedAtBetween(Date fromDateTime, Date toDateTime) {
        return logMapper.toLogDTOList(logRepository.findAllByCreatedAtBetween(fromDateTime, toDateTime));
    }

    public LogDTO create(LogDTO logDTO) {
        Log log = logRepository.save(logMapper.toLog(logDTO));
        return logMapper.toLogDTO(log);
    }
}
