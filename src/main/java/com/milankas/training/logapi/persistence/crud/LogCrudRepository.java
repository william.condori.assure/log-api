package com.milankas.training.logapi.persistence.crud;


import com.milankas.training.logapi.persistence.entity.Log;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;
import java.util.UUID;

public interface LogCrudRepository extends JpaRepository<Log, UUID> {

    List<Log> findAllByServiceName(String serviceName);

    List<Log> findAllByLevelIn(List<String> levelList);

    List<Log> findAllByMessageContains(String message);

    List<Log> findAllByCreatedAtBetween(Date fromDateTime, Date toDateTime);

}
