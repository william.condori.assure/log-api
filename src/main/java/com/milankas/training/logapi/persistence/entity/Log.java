package com.milankas.training.logapi.persistence.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "logs")
public class Log {

    @Id
    @GeneratedValue
    private UUID id;
    private String level;
    @Column(name = "created_at")
    @CreationTimestamp
    private Date createdAt;
    @Column(name = "service_name")
    private String serviceName;
    private String message;

}
