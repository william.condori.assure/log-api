package com.milankas.training.logapi.persistence.mapper;

import com.milankas.training.logapi.domain.dto.LogDTO;
import com.milankas.training.logapi.persistence.entity.Log;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface LogMapper {

    LogDTO toLogDTO(Log log);
    List<LogDTO> toLogDTOList(List<Log> logList);

    @Mapping(target = "createdAt", ignore = true)
    Log toLog(LogDTO logDTO);

}
